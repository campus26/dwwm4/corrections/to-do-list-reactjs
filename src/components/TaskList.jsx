import {Task} from './Task';

export const TaskList = ({ tasks, toggleCompletion, deleteTask }) => {

  const completedTasks = tasks.filter((task) => task.completed);//filtre qui met dans un tableau la liste des tasks complete
  const incompleteTasks = tasks.filter((task) => !task.completed);//filtre qui met dans un tableau la liste des tasks incomplete

  return (
    <>
      <ul>
        {incompleteTasks.map((task) => (
          <Task key={task.id} task={task} toggleCompletion={toggleCompletion} deleteTask={deleteTask}/>
        ))}
      </ul>
      <hr />
      <ul>
        {completedTasks.map((task) => (
          <Task key={task.id} task={task} toggleCompletion={toggleCompletion} deleteTask={deleteTask}/>
        ))}
      </ul>
    </>
  );
};
