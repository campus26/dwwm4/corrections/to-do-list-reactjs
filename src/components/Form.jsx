import { useState } from 'react'

export function Form({ addTask }) {
   
  //newTask détient les données a ajouter
    const [newTask, setNewTask] = useState('');

    //attribut la valeur de l'input a son changement newTask
    const handleInputChange = (e) => {
      setNewTask(e.target.value);
    };
  
    //Au clique sur le submit
    const handleSubmit = () => {
      //si pas vide
      if (newTask.trim() != '') {
        addTask(newTask);//appeler la fonction d'ajout
        setNewTask('');//remettre la task vide
      }
    };
  
    return (
      <div className='addForm'>
        <input
          type="text"
          value={newTask}
          onChange={handleInputChange}
          placeholder="Ajouter une tâche"
        />
        <button onClick={handleSubmit}>
          <span></span>
          <span></span>
        </button>
      </div>
    );
}
  