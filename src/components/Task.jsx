import { RiDeleteBin6Fill } from "react-icons/ri";

export function Task({ task, toggleCompletion, deleteTask}) {
    return (
        <li key={task.id}>
         <label style={{ 
            textDecoration: task.completed ? 'line-through' : 'none',
            fontWeight: task.completed ? 'normal' : '600', // Utilise fontWeight pour changer le gras du texte
            color: task.completed ? '#a8a8a8' : '#1F1F20', // Choisit une couleur grise claire pour le texte complété
          }}>
            <input
              type="checkbox"
              checked={task.completed}
              onChange={() => toggleCompletion(task.id)}
            />
            {task.text}
          </label>
          <button className="deleteTaskBtn" onClick={() => deleteTask(task.id)}><RiDeleteBin6Fill  style={{ fontSize: '30px', color: '#DC143C' }}/></button>
        </li>
      );
}
  