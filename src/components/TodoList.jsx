import { useState ,useEffect} from 'react'
import { Form } from './Form'
import { TaskList} from './TaskList'
import { PiCalendarFill } from "react-icons/pi";
import moment from 'moment';

export function TodoList({todoList,name,date}) {
    moment.locale('fr');

    // Récupère les tâches du localStorage ou une liste vide si elles n'existent pas
    const initialTasks = JSON.parse(localStorage.getItem(`tasks_${todoList.id}`)) || [];
 
    // Utilise initialTasks comme état initial
    //tasks est un tableau d'objet task
    const [tasks, setTasks] = useState(initialTasks);
    //filtre
    const [filter, setFilter] = useState('all'); // 'all', 'completed', 'incomplete'

    //utiliser le useEffect pour mettre à jour le localstorage à chaque changemant de la liste globale des todoliste
    useEffect(() => {
        localStorage.setItem(`tasks_${todoList.id}`, JSON.stringify(tasks));
      }, [tasks]);

    //fonction pour ajouter une nouvelle tache (fonction utilisé dans le form)
    const addTask = (newTaskText) => {
        setTasks([...tasks, { id: Date.now(), text: newTaskText, completed: false }]);//on donne un id timestamp du moment pour l'identifier et pour aider react à distinguer les éléments de listes
    };

    //fonction pour switch l'etat de completion d'une task (fonction utilisé dans les task)
    const toggleCompletion = (taskId) => {
        setTasks(
        tasks.map((task) =>
            task.id == taskId ? { ...task, completed: !task.completed } : task
        )
        );
    };

    //fonction pour supprimer une task (fonction utilisé dans les task)
    const deleteTask = (taskId) => {
        setTasks(tasks.filter((task) => task.id != taskId));
    };

    //filtre des tasks
    const filteredTask = tasks.filter((task) => {
    
        switch (filter) {
        case 'completed':
            return task.completed //active le filtre des tache complète
        case 'incomplete':
            return !task.completed //active le filtre des tache incomplète
        default:
            return true; //pas de filtre
        }

    })

    
    const today = moment().format('D MMM YYYY');
    const isToday = moment(date).isSame(today, 'day');

    return (
        <>
            <div className='todoHead'>
            <h1>{name}</h1>
            <div>
                <div>
                <h2>{isToday ? "Aujourd'hui" : moment(date).format('dddd')}</h2>
                <span>{moment(date).format('D MMM YYYY')}</span>
                </div>
                <PiCalendarFill style={{ fontSize: '45px', color: '#292DE3' }}/>
            </div>
            </div>
            <div className="filter">
            <button onClick={() => setFilter("all")}>Tout</button>
            <button onClick={() => setFilter("incomplete")}>A Faire</button>
            <button onClick={() => setFilter("completed")}>Terminé</button>
            </div>
        
            <hr />
            <Form addTask={addTask} />
            <TaskList tasks={filteredTask} toggleCompletion={toggleCompletion} deleteTask={deleteTask} />
        </>
    );
}


