import './App.css'
import {TodoList} from './components/TodoList'
import { useState ,useEffect} from 'react'
import moment from 'moment';

// moment.now().format('D MMM YYYY')


function App() {
  moment.locale('fr');

  // Récupère toutes les listes de tâches du localStorage ou une liste vide si elles n'existent pas
  const initialTodoLists = JSON.parse(localStorage.getItem('todoLists')) || [];

  const [todoListes, setTodoListes] = useState(initialTodoLists);

  const [newTodoList, setNewTodoList] = useState({name:'Todo',date: moment().format("YYYY-MM-DD")});

  useEffect(() => {
    console.log(todoListes);
    localStorage.setItem('todoLists', JSON.stringify(todoListes));
  }, [todoListes]);

  const addTodoList = (name, date) => {
    // Ajouter une nouvelle liste avec un nom et une date
    setTodoListes([...todoListes, { id: Date.now(), name, date }]);
  };

  const removeTodoList = (todoListId) => {
    // Supprimer la liste et ses task du localstorage en fonction de son identifiant
    localStorage.removeItem(`tasks_${todoListId}`);
    setTodoListes(todoListes.filter((list) => list.id !== todoListId));
  };

   //attribut la valeur de l'input a son changement newTask
  const handleInputChange = (e) => {
    const { name, value } = e.target;
    // Met à jour l'objet newTodoList avec la nouvelle valeur
    setNewTodoList((prevTodoList) => ({
      ...prevTodoList,
      [name]: value,
    }));
  };

  //Au clique sur le submit
  const handleSubmit = () => {
    //appeler la fonction d'ajout
    addTodoList(newTodoList.name, newTodoList.date);
    setNewTodoList({name:'Todo',date: moment().format("YYYY-MM-DD")});//remettre par défaut
  };

  return (
    <> 

     <div className='addForm addtodo'>
        <input
          type="text"
          value={newTodoList.name}
          name="name"
          onChange={handleInputChange}
          placeholder="Nom nouvelle todoList"
        />
        <input type="date" value={newTodoList.date} name="date" onChange={handleInputChange} />
        <button onClick={handleSubmit}>
          <span></span>
          <span></span>
        </button>
      </div>

      <div className='ListeTodoListe'>
        {
          todoListes.map((todoList) => (
            <div className='todoList' key={todoList.id}>
              <TodoList todoList={todoList} name={todoList.name} date={todoList.date}/>
              <button onClick={() => removeTodoList(todoList.id)}>Supprimer TodoList</button>
            </div>
          ))
        }
      </div>
    </>
  );
}

export default App
